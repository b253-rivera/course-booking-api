const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");

// // Route for creating a course
// router.post("/", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
// });


//========================================Activity
// router.post("/", auth.verify, (req,res) => {

// 	const adminId = auth.decode(req.headers.authorization).isAdmin;

// 	courseController.createCourse(req.body, adminId).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
// })

// 
//==============================================ms janie code
router.post("/", auth.verify, (req, res) => {

	// const data = {
	// 	course: req.body,
	// 	isAdmin: auth.decode(req.headers.authorization).isAdmin
	// }

	// courseController.addCourse(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

// Routes for retrieving all the courses

router.get("/all", auth.verify, (req, res) => {

	const adm = auth.decode(req.headers.authorization);

	if(adm.isAdmin){
	courseController.getAllCourses().then(resultFromController => res.send (resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});

// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses

router.get("/", (req,res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

// Retrieving a specific course
		/*
			Steps:
			1. Retrieve the course that matches the course ID provided from the URL
		*/

router.get("/:courseId", (req,res) => {

	console.log(req.params);
	// Route for retrieving a specific course
	// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
	// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
	// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
})

// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course

router.put("/:courseId", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
	} else {
		res.send(false);
	}
})


//===========================activity

// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently

router.patch("/:courseId", auth.verify, (req,res) => {

	const adminData = auth.decode(req.headers.authorization);

	if(adminData.isAdmin){
		courseController.updateAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
	} else {
		res.send(false);
	}
})



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;